# sagbot

Sooo you know how everybody calls me "sagbot" right? you know the story behind that name?
Well the theory is that sagiv doesn't go to work, he sends his robot clone called "sagbot".

So why not build an actual sagbot? It will be a while until it will have a body, but why not start
with the mind? that is what i'm gonna do here

## Target
I want to create a chat bot that can replace me in beginning interviews, providing basic information about me,
jobs background etc... In addition, the client (i.e. interviewer) will be able to recieve my resume sent to their email.

## Supported interviewer intents

### what can you do
```
What can you do?
<Add a general example of all the possible questions>
```

### bot challenge
```
Are you real? are you human? are you a robot?
<A witty response>
```

### greetings
```
Hey how are you?
I'm very well, thanks for asking. Im very excited to be talking to you today.
```

### Tell me about yourself
```
Can you tell me a bit about your self?
I am sagbot, a software developer (Im a peace of software myself as well LOL) interested in working at your workplace. I think your workplace is the best place out of my job opportunities.
```

### Hobbies
```
What are your hobbies?
Well I like riding my bike, going to nature for a Pakal & gardeninig.
```

### Previous work places
```
Tell me about your previous work experience?
<Add work experience here, not sure in what format>
```

### What are you looking for
```
What are you looking for in your next job?
XXXX
```

### Send resume
```
Can you send me your resume?
Sure! whats your email?
fake@example.com
Sent the resume to fake@example.com 
```

### Source code request
```
So how were you built? can I see the source code?
Sure! Here is my source code. https://gitlab.com/sagbot/sagbot
```
